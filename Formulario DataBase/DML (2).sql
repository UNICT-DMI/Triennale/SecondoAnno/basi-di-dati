--DML (DATA MANIPOLATION LANGUAGE)-------------------------------------------------------------

/*INSERT*/
INSERT INTO Schema1 --inserisce i dati indicati su VALUES nella tabella Schema1
VALUES ('a1', 01),('a2',02)
--se la lista non include tutti gli attributi, i restanti assumono valore NULL (se ammesso) o il valore di default (se specificato)

/*UPDATE*/
UPDATE Schema1
SET a = 126 --aggiorna a 3 in 126 della tabella Schema1 
WHERE a = 3
--il comando generico senza WHERE aggiorna tutte le righe della tabella

/*DELETE*/
DELETE FROM Schema1 --cancella 123456 dell'attributo a dalla tabella Schema1
WHERE a = 123456

/*ALTER TABLE*/
--usato per aggiungere una colonna, rimuovere una colonna, modificare il nome di una colonna , modificare il tipo di dati di una colonna

ALTER TABLE Schema1 ADD attributo char(datetime); --inserisce in nuovo attributo di tipo char
ALTER TABLE Customer CHANGE Address Addr char(50); --modifica il nome di “Address” in “Addr”
ALTER TABLE Customer MODIFY Addr char(30); --modifica il tipo di dati di “Addr” in char(30)
ALTER TABLE Customer DROP Gender; --elimina la conolla Gender 

/*VINCOLI DI INTEGRITA'*/
--proprietà che devono essere soddisfatte dalle istanze di una base di dati; è una condizione booleana che deve essere soddisfatta per non far fallire la transizione

--Vincoli intrarelazionali: vincoli che interessano una sola tabella

h char(16) NOT NULL --l'attributo h non può assumere il valore NULL
h smallint DEFAULT 1 --se al momento dell’inserimento di una tupla non viene inserito il valore dell’attributo h, verrà automaticamente assegnato il valore di default 1
h INTEGER NOT NULL CHECK (18<= Voto AND Voto<=31) --l’attributo h è un intero, non può assumere il valore NULL, e deve soddisfare il limite compreso tra 18 e 31. (CHECK = controlla che…)
CREATE TABLE Schema1(id CHAR(6) PRIMARY KEY,…) --una tupla della tabella Schema1 è identificata unicamente dall’attributo id. Massimo una chiave primaria per tabella

--Vincoli interrelazionali: vincoli che definiscono legami tra due o più tabelle

CREATE TABLE Schema1(
a CHAR(6) NOT NULL PRIMARY KEY,
b DATE NOT NULL,
c INTEGER NOT NULL REFERENCES Schema2(c), --c referenzia c di un'altra tabella
d CHAR(2),
e Char(6),
FOREIGN KEY (f,g) REFERENCES Schema3(f,g) --f e g referenziano f e g di un'altra tabella
)

--Vincoli su chiave esterne
ON DELETE NO ACTION --rifiuta l’operazione (la più diffusa nei DBMS).
ON DELETE CASCADE --cancella tutte le n-uple con valori della chiave esterna corrispondenti alla chiave primaria delle nuple cancellate
ON DELETE SET NULL --assegna il valore NULL agli attributi della chiave esterna
