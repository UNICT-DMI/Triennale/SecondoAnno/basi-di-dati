--TRANSAZIONI-------------------------------------------------------------
--una transazione è un’unità logica di elaborazione che corrisponde a una serie di operazioni fisiche elementari (letture/scritture) sul DB

UPDATE CC 
SET Saldo = Saldo - 50 
WHERE Conto = 123 

UPDATE CC 
SET Saldo = Saldo + 50 
WHERE Conto = 253

--tutte le operazioni devono essere eseguite

/* PROPRIETA' ACID*/
--Atomicity: una transazione è un’unità di elaborazione; il DBMS garantisce che la transazione venga eseguita come un tutt’uno (per intero o per niente)
--Consistency: una transazione lascia il DB in uno stato consistente; il DBMS garantisce che nessuno dei vincoli di integrità del DB venga violato (Es. Durante l'esecuzione ci possono essere errori come la violazione del limite di denaro trasferibile, ma se questi errori restano alla fine allora la transazione deve essere annullata per intero, o abortita)
--Isolation: una transazione esegue indipendentemente dalle altre; se più transazioni eseguono in concorrenza, il DBMS garantisce che l’effetto netto è equivalente a quello di una qualche esecuzione sequenziale delle stesse (Es. Se due assegni emessi sullo stesso conto corrente vengono incassati contemporaneamente si deve evitare di trascurarne uno)
--Durability: gli effetti di una transazione che ha terminato correttamente la sua esecuzione devono essere persistenti nel tempo; il DBMS deve proteggere il DB a fronte di guasti


CONNECT TO Sample 
SELECT * FROM Department 
INSERT INTO Department(DeptNo,DeptName,AdmrDept) VALUES ('X00','nuovo dept 1','A00') 
SAVEPOINT pippo ON ROLLBACK RETAIN CURSORS SELECT * FROM Department -- per vedere che succede 
INSERT INTO Department(DeptNo,DeptName,AdmrDept) VALUES ('Y00','nuovo dept 2','A00') 
SELECT * FROM Department 
ROLLBACK WORK TO SAVEPOINT pippo --abortisce l'operazione e torna al punto di salvataggio
SELECT * FROM Department 
COMMIT WORK --comunica il termine delle operazioni