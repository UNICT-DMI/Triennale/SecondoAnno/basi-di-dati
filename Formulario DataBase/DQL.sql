--DQL (DATA QUERY LANGUAGE)-------------------------------------------------------------

/*SELECT*/
SELECT m, n --mostra cosa si vuole come risultato
SELECT* --tutti gli attributi
SELECT m, m+10 --crea un nuovo attributo con dati calcolati
SELECT m AS n, p-10 r --rinomina gli attributi
SELECT Tabella.Attributo --referenziazione
SELECT Nome CONCAT''CONCAT Cognome --combinazione di due attributi in uno
SELECT DISTINCT m --elimina dai risultati le righe dublicate

/*FROM*/
FROM t -- indica da dove si prendono i dati

/*WHERE*/
WHERE a=10 --indica che condizioni la query deve soddisfare limitando le righe tramite una condizione
WHERE a>10 AND b=6 --si possono utilizzare gli operatori logici (AND, OR NOT)  e di confronto (<,>,=,>=,<=,<>)
WHERE c BETWEEN 10 AND 20 --range estremi inclusi
WHERE c IN (34,35,36) --attributo incluso in una lista
WHERE c LIKE '_A%'--ricerche WILD CARD: _ denota un carattere, % denota zero o più caratteri
WHERE c IS NULL / WHERE c IS NOT NULL --esistenza o meno dei valori nulli

/*ORDER BY*/
ORDER BY d ASC --ordina il risultato secondo una o più colonne in ordine ASC o DESC

/*JOIN*/
WHERE a.id = b.id --forma implicita, Crea una nuova tabella combinando i valori delle due tabelle di partenza usando un attributo comune
FROM a JOIN b ON (a.idA<b.idB) --forma esplicita
FROM a JOIN b USING (id) --equiJOIN
FROM a,b --prodotto cartesiano implicito
FROM a CROSS JOIN b --prodotto cartesiano esplicito
FROM a LEFT OUTER JOIN b --contiene sempre tutti i record della tabella di sinistra, se non trova corrispondenza a destra mette NULL
FROM a RIGHT OUTER JOIN b --contiene sempre tutti i record della tabella di destra, se non trova corrispondenza a sinistra mette NULL
FROM a FULL OUTER JOIN b --contiene sempre tutti i record delle tabelle, se non trova corrispondenze mette NULL

/*OPERATORI AGGREGATI*/
-- tutti i seguenti operatori danno un solo risultato, non è possibile dunque inserire nella select list altri elementi che non siano a loro volta operatori aggregati
SELECT AVG(e) --media di di tutti i valori dell'attributo e
SELECT SUM(e) --somma di tutti i valori dell'attributo e
--AVG e SUM usati solo su dati numerici
SELECT MIN(e) --il valore minimo di e
SELECT MAX (e) --il valore massimo di e 
COUNT(e) --numero di righe totali dell'attributo e
COUNT(*) --numero di righe totali della tabella
-- COUNT non tiene conto dei valori NULL

/*GROUP BY*/
SELECT a, COUNT(b) AS totb ... GROUP BY a --mostra quante sono le b per ogni a con risultati vicini in funzione ad a
HAVING MAX(f)>100--usata per restringere i gruppi quando le righe sono raggruppate
--  HAVING va usato solamente per specificare un criterio di raggruppamento o di aggregazione (MAX, MIN, COUNT,ecc.)

/*SUBQUERY*/
SELECT ... WHERE g=(SELECT ...) --la subquery passa i risultati alla query esterna che ne confronta i risultati nella WHERE
SELECT ... WHERE g>(SELECT ...) --gli operatori di confronto classici (=, <,…) si possono usare solo se la subquery restituisce un solo valore (nel caso in cui usa AVG, MAX, ecc.)
SELECT ... WHERE g=ANY(SELECT ...) --la condizione è vera se g vale per almeno uno dei valori della subquery
SELECT ... WHERE g=ALL(SELECT ...) --la condizione è vera se g vale per tutti i valori della subquery
SELECT ... WHERE g IN(SELECT ...) --la condizione è vera se g vale per almeno uno dei valori della subquery 
SELECT ... WHERE g NOT IN(SELECT ...) --la condizione è vera se g non vale neanche uno dei valori della subquery
SELECT ... WHERE EXISTS(SELECT ...) --la condizione è vera se la subquery restituisce qualcosa
SELECT ... WHERE NOT EXISTS(SELECT ...) --la condizione è vera se la subquery non restituisce qualcosa
--Non è possibile fare riferimenti a variabili definite in blocchi più interni
--Se la subquery fa riferimento a variabili definite in un blocco esterno, allora si dice che è correlata
--Se un nome di variabile è omesso, si assume riferimento alla variabile più “vicina”

/*VISTE*/
--tabelle ausiliare virtuali
CREATE VIEW H(a,b) AS SELECT... --crea una tabella virtuale H contenente gli attributi a e b

DROP (TABLE | VIEW) Nome [RESTRICT|CASCADE] --cancellazione vista
--con RESTRICT non viene cancellata se è utilizzata in altre viste
--con CASCADE verranno rimosse tutte le viste che usano la View o la Tabella rimossa
--la distruzione di una VIEW non altera le tabelle su cui la VIEW si basa

WITH RECURSIVE Antenati(Persona,Avo) AS --vista ricorsiva che trova tutti gli antenati di Anna
((SELECT Figlio, Genitore -- subquery base FROM Genitori) 
UNION ALL (SELECT G.Figlio, A.Avo -- subquery ricorsiva 
FROM Genitori G, Antenati A 
WHERE G.Genitore = A.Persona)) 
SELECT Avo 
FROM Antenati 
WHERE Persona = ‘Anna’